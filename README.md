# dotfiles

a collection of my useful configurations, scripts, and other stuff for linux.

- Note that some scripts need to reference other files in this repository, so this folder is expected to be in ~/Scripts/Dotfiles, although this can easily be configured by editing the first lines of any such script

- the ~ folder contains files that normally go in the $HOME
