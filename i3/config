gaps outer 10
gaps inner 5
set $alt Mod1

#Requires
#	redshift
#	feh (background)

# i3 config file (v4)

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8
font pango:Fira Mono 8

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Shift+q kill
bindsym $mod+q kill
bindsym $alt+F4 kill
bindsym $alt+q kill

# start dmenu (a program launcher)
#bindsym $mod+d exec dmenu_run
bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# change focus
#bindsym $mod+j focus left
#bindsym $mod+k focus down
#bindsym $mod+l focus up
#bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
#bindsym $mod+Shift+j move left
#bindsym $mod+Shift+k move down
#bindsym $mod+Shift+l move up
#bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
#bar {
#        status_command i3status
#}
#gold
#programs
bindsym $alt+t exec --no-startup-id xterm
bindsym $mod+l exec ~/Scripts/Dotfiles/i3lock/lock.sh
bindsym $mod+Return exec konsole
bindsym F2 exec --no-startup-id rofi -show run
bindsym shift+Print exec 'gnome-screenshot -a -f /tmp/area.png; xclip -selection clipboard -t image/png -i /tmp/area.png'
bindsym Print exec --no-startup-id ~/Scripts/Dotfiles/DE-Scripts/screen_shot.sh
bindsym $alt+r exec --no-startup-id ~/Scripts/Misc/ThisAmericanLife.sh
bindsym $alt+d exec --no-startup-id ~/Scripts/Misc/TheDailyShow.sh
bindsym $alt+Control+t exec --no-startup-id ~/Scripts/Misc/torrent.sh
bindsym $alt+i exec --no-startup-id konsole -e /home/$USER/Scripts/Torrents/wrapperPause.sh
bindsym $alt+Control+s exec --no-startup-id konsole -e /home/$USER/Scripts/Torrents/Search/searchTPB.sh
bindsym $alt+Control+h exec --no-startup-id konsole -e /home/$USER/Git/local-configs/whitelistHost.sh
bindsym $alt+Control+a exec --no-startup-id konsole -e "cmus"

#safety
bindsym $alt+Tab workspace 1
bindsym $alt+z workspace 1
bindsym $alt+a exec --no-startup-id ~/Scripts/Misc/killall.sh

#Startup
#Tray
exec --no-startup-id parcellite
exec --no-startup-id redshift-gtk -l 28.802861:-81.269453
exec --no-startup-id wicd-gtk -t
#Lock Screen on Startup
exec --no-startup-id ~/Git/i3lock-fancy/lock -t ""
#Find and Set Rokuip on Startup
exec --no-startup-id ~/Scripts/Misc/set_roku_ip.sh

#Random Background
exec_always --no-startup-id ~/Scripts/Dotfiles/DE-Scripts/random_wallpaper.sh

#media keys
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 5
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 5
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -q sset Master 2%+
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -q sset Master 2%-
bindsym XF86AudioMute exec --no-startup-id amixer -q set Master toggle

#i3 custom settings
hide_edge_borders both
# class                 border  backgr. text    indicator child_border
client.focused          #000000 #000000 #ffffff #000000   #000000
client.focused_inactive #DADADA #DADADA #000000 #DADADA   #DADADA
client.unfocused        #DADADA #DA##################] 100%
 xfce4-clipman-pl...   145.7 KiB  5.93 MiB/s 00:00 [##DADA #000000 #DADADA   #DADADA
client.urgent           #C88888 #C88888 #000000 #C88888   #C88888
client.placeholder      #DADADA #DADADA #000000 #DADADA   #DADADA
client.background       #ffffff
new_window pixel 0

#dynamic configuration
#bindsym $alt+Control+b exec --no-startup-id konsole -e /home/$USER/Scripts/Dotfiles/i3/dynamic.sh
bindsym $alt+Control+b exec --no-startup-id ~/Scripts/Dotfiles/i3/dynamic.sh
#cmus controls
bindsym $alt+Right exec --no-startup-id cmus-remote -n
bindsym $alt+Left exec --no-startup-id cmus-remote -r
bindsym $alt+p exec --no-startup-id cmus-remote -u
bindsym $alt+Up exec --no-startup-id cmus-remote -C "vol +5%"
bindsym $alt+Down exec --no-startup-id cmus-remote -C "vol -5%"
bindsym Control+Shift+Right exec --no-startup-id cmus-remote -C "seek +5"
bindsym Control+Shift+Left exec --no-startup-id cmus-remote -C "seek -5"
bindsym $alt+x exec --no-startup-id cmus-remote -C "toggle repeat_current"

#Compositor
exec_always --no-startup-id killall compton xcompmgr; compton

#Polybar
exec_always --no-startup-id `killall polybar; sleep 1; polybar main`
