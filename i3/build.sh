#!/bin/bash
#vars contains default variables, override should be created by dynamic.sh

#source ./utils/vars
. ./utils/vars
#source ./utils/override
. ./utils/override


echo "Polybar: $polybar"
echo "PolybarNews: $polybarNews"
echo "Terminal: $terminal"
echo "Playerctl: $playerctl"
echo "Spotify: $spotify"
echo "Xcompmgr: $xcompmgr"
echo "Compton: $compton"

cat ./utils/base > ~/.config/i3/config.tmp



#playerctl > spotify > cmusHeadphones > panicHeadphones > cmus
if [ "$playerctl" = true ] ; then
    cat ./utils/playerctl-controls >> ~/.config/i3/config.tmp
elif [ "$spotify" = true ] ; then
    cat ./utils/spotify-controls >> ~/.config/i3/config.tmp
elif [ "$cmusHeadphone" = true ] ; then
    cat ./utils/cmus-headphone-controls >> ~/.config/i3/config.tmp
elif [ "$panicHeadphone" = true ] ; then
    cat ./utils/panic-headphone-controls >> ~/.config/i3/config.tmp
else
    cat ./utils/cmus-controls >> ~/.config/i3/config.tmp
fi

#compton > xcompmgr > na
if [ "$compton" = true ] ; then
    echo "" >> ~/.config/i3/config.tmp
    echo "#Compositor" >> ~/.config/i3/config.tmp
    echo "exec_always --no-startup-id killall compton xcompmgr; compton" >> ~/.config/i3/config.tmp
elif [ "$xcompmgr" = true ] ; then
    echo "" >> ~/.config/i3/config.tmp
    echo "#Compositor" >> ~/.config/i3/config.tmp
    echo "exec_always --no-startup-id killall compton xcompmgr; xcompmgr" >> ~/.config/i3/config.tmp
fi

#polybarNews > polybar > na
if [ "$polybarNews" = true ]; then
    echo "" >> ~/.config/i3/config.tmp
    echo "#PolybarNews" >> ~/.config/i3/config.tmp
    echo "exec_always --no-startup-id \`killall polybar; /usr/bin/polybar newsmain\`" >> ~/.config/i3/config.tmp
elif [ "$polybar" = true ] && [ "$statusbar" = true ]; then
    echo "" >> ~/.config/i3/config.tmp
    echo "#Polybar" >> ~/.config/i3/config.tmp
    echo "exec_always --no-startup-id \`killall polybar; /usr/bin/polybar main & polybar status\`" >> ~/.config/i3/config.tmp
elif [ "$polybar" = true ]; then
    echo "" >> ~/.config/i3/config.tmp
    echo "#Polybar" >> ~/.config/i3/config.tmp
    echo "exec_always --no-startup-id \`killall polybar; /usr/local/bin/polybar main\`" >> ~/.config/i3/config.tmp
elif [ "$statusbar" = true ]; then
    echo "" >> ~/.config/i3/config.tmp
    echo "#Statusbar" >> ~/.config/i3/config.tmp
    echo "exec_always --no-startup-id \`killall polybar; /usr/bin/polybar status\`" >> ~/.config/i3/config.tmp
fi

cat ~/.config/i3/config.tmp | sed "s/mate-terminal/$terminal/" > ~/.config/i3/config

killall xcompmgr compton polybar > /dev/null 2>&1
i3-msg restart

#
if [ "$periodictable" = true ] ; then
  echo 'a'
fi
#echo ""
