#!/bin/bash
#requires zenity
#creates ./utils/override variables
echo $USER
cd /home/$USER/Scripts/Dotfiles/i3
echo $PWD
echo "#Created By Dynamic.sh" > ./utils/override
options=`zenity --height=250 --list --checklist --title='i3 Configuration - CM' --text='Dynamic Configuration Options' --column=Options --column=Choice FALSE playerctl FALSE spotify FALSE cmus-headphone FALSE panic-headphone TRUE mate-terminal FALSE konsole FALSE gnome-terminal FALSE xcompmgr TRUE compton TRUE polybarNews FALSE polybar FALSE periodic-table FALSE statusbar --separator=':'`

if [[ $options == *"cmus-headphone"* ]]; then
  echo "cmusHeadphone=true" >> ./utils/override
else
  echo "cmusHeadphone=false" >> ./utils/override
fi

if [[ $options == *"panic-headphone"* ]]; then
  echo "panicHeadphone=true" >> ./utils/override
else
  echo "panicHeadphone=false" >> ./utils/override
fi

if [[ $options == *"playerctl"* ]]; then
  echo "playerctl=true" >> ./utils/override
else
  echo "playerctl=false" >> ./utils/override
fi

if [[ $options == *"spotify"* ]]; then
  echo "spotify=true" >> ./utils/override
else
  echo "spotify=false" >> ./utils/override
fi

if [[ $options == *"polybarNews"* ]]; then
  echo "polybarNews=true" >> ./utils/override
  echo "polybar=false" >> ./utils/override
elif [[ $options == *"polybar"* ]]; then
  echo "polybar=true" >> ./utils/override
else
  echo "polybarNews=false" >> ./utils/override
  echo "polybar=false" >> ./utils/override
fi

if [[ $options == *"statusbar"* ]]; then
  echo "statusbar=true" >> ./utils/override
else
  echo "statusbar=false" >> ./utils/override
fi

if [[ $options == *"mate-terminal"* ]]; then
  echo "terminal=mate-terminal" >> ./utils/override
elif  [[ $options == *"konsole"* ]]; then
  echo "terminal=konsole" >> ./utils/override
elif  [[ $options == *"gnome-terminal"* ]]; then
  echo "terminal=gnome-terminal" >> ./utils/override
fi


if [[ $options == *"xcompmgr"* ]]; then
  echo "xcompmgr=true" >> ./utils/override
  echo "compton=false" >> ./utils/override
elif  [[ $options == *"compton"* ]]; then
  echo "compton=true" >> ./utils/override
  echo "xcompmgr=false" >> ./utils/override
else
  echo "compton=false" >> ./utils/override
  echo "xcompmgr=false" >> ./utils/override
fi

if [[ $options == *"periodic-table"* ]]; then
  echo "periodictable=true" >> ./utils/override
fi

sh build.sh
sleep 1
exit
