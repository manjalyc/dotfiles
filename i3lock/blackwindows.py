def setSkips(x1 , y1 , x2 , y2):
	skip[y1:y2,  x1:x2] = 1

def hN(x , y):
	cont = 1
	while x + 1 < w1x and cont:
		x += 1
		if a1[y][x][0] == a2[y][x][0]:
			cont = 0
	return x
	
def hV(x , y):
	cont = 1
	while y + 1 < w1y and cont:
		y += 1
		if a1[y][x][0] == a2[y][x][0]:
			cont = 0
	return y

from PIL import Image
import numpy as np
w1 = Image.open("wallpaperScreenshot.png")
w2 = Image.open("screenshot.png")
w1x = w1.size[0]
w1y = w1.size[1]
a1 = np.array(w1)
a2 = np.array(w2)
skip = np.zeros((w1y,w1x))



for x in range(w1x):
	for y in range(w1y):
		if( not skip[y][x] ):
			if a1[y][x][0] != a2[y][x][0] or a1[y][x][1] != a2[y][x][1] or a1[y][x][2] != a2[y][x][2]:
				setSkips(x,y,hN(x,y),hV(x,y))

for x in range(w1x):
	for y in range(w1y):
		if skip[y][x]:
			a2[y][x][0] = 0
			a2[y][x][1] = 0
			a2[y][x][2] = 0

im = Image.fromarray(a2)
im.save("lockscreen.png")