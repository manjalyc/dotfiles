from PIL import Image
import numpy as np
w1 = Image.open("../StartupLock.png")
w2 = Image.open("screenshot.png")
w1x = w1.size[0]
w1y = w1.size[1]
a1 = np.array(w1)
a2 = np.array(w2)
a3 = list(a2)
step = 8
for x in range(0,w1x,step):
	for y in range(0,w1y,step):
		pixelate = 0
		iy = -1;
		ix = -1;
		for a in range (step):
			for b in range (step):
				if x+a < w1x and y+b < w1y:
					if a1[y+b][x+a][0] != a2[y+b][x+a][0] or a1[y+b][x+a][1] != a2[y+b][x+a][1] or a1[y+b][x+a][2] != a2[y+b][x+a][2]:
						pixelate = 1
						iy = y + b
						ix = x + b
						break
		if pixelate:
			for a in range (0,step,1):
				for b in range (0,step,1):
					if x+a < w1x and y+b < w1y:
						a3[y+b][x+a] = a2[iy][ix]
			
					
im = Image.fromarray(a2)
im.save("./lockscreen.png")
