#requires 
#sudo pip install pillow numpy
from PIL import Image
import sys
import numpy as np
#w1 = Image.open("wallpaperScreenshot.png")
filename="screenshot.png"
if len(sys.argv) > 0:
	filename = sys.argv[len(sys.argv)-1]
	print(filename)
w = Image.open(filename)
wx = w.size[0]
wy = w.size[1]
arr = np.array(w)
skip = np.zeros((wy,wx))



#Pixelate
step = 8 #U need to figure this out for different displays
mid = int(step/2)
xmid,ymid = mid, mid
for x in range(0,wx):
	for y in range(0,wy):
		if (x + mid) % step == 0: xmid = x
		if (y + mid) % step == 0: ymid = y
		#print(y,x,xmid,ymid)
		arr[y][x] = arr[ymid][xmid]
"""
for xmid in range(0, wx, step):
	for ymid in range(0, wy, step):
		for x in range(xmid-mid, xmid+mid):
			for y in range(ymid-mid, ymid+mid): arr[y][x] = arr[ymid][xmid]

xmid += step
ymid += step
for x in range(xmid-step, wx):
	for y in range(ymid-step, wy): arr[y][x] = arr[ymid][xmid]
"""

im = Image.fromarray(arr)
im.save(filename)
