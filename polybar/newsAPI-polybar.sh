#!/bin/bash
#A little script meant to display news headlines in polybar
#Requires jq to be installed (in most distrobutions the package is simply called jq)

#newsapi.org
newsAPIKey="your-api-key" #You'll have to get an api key
newsAPIUpdateInterval=60 #Seconds between updating all headlines, (approximate)
intervalBetweenHeadline=1 #Seconds between displaying each headline
rightPadding=" "
leftPadding=" "

#You can delete this line, it sources my settings for the above variables
source $(eval echo "~")"/Scripts/sources"

if [[ "$*" == *"-h"* ]]; then
	echo "Usage: "
	echo "  ./newsAPI.sh [source 1] [source 2] [source 3]..."
	echo "Ex:"
	echo "  ./newsAPI.sh \"bbc-news\" \"ars-technica\""
	exit
fi

#location of where the current article's url will be
mkdir -p /tmp/polybar
urlFile=$(echo $@ | sed 's/ /-/g')
urlFile="/tmp/polybar/newsapi-polybar-url-$urlFile"
#title output file
titleFile="/tmp/polybar/newsapi-polybar-title-$urlFile"


#gets current polybar's pid for ipc
polybarPID=$(cat /tmp/polybar/polybarnewsapi-ipc-pid)
urlFile="/tmp/polybar/polybar-$polybarPID-newsapi-url"
titleFile="/tmp/polybar/polybar-$polybarPID-newsapi-title"
#pid output file for polybar to kill the sleep to trigger next article
echo $$ >> "/tmp/polybar/polybar-$polybarPID-newsapi-scriptPID"


sourcesArray=( "$@" )
numSources=${#sourcesArray[@]}
hC=0
hM=1
while true; do
	#get/update headlines
	echo "dD" > /tmp/d
	updateIf0=$(( hC % hM ))
	if [ $updateIf0 -eq 0 ]; then
		titles=()
		urls=()
		i=0
		while [ $i -lt ${#sourcesArray[@]} ]; do
			newsAPISource=${sourcesArray[$i]}
			echo "Updating Headlines from [$newsAPISource].." > $titleFile
			polybar-msg -p $polybarPID hook newsapi 2
			echo "" > $urlFile
			jsonHeadlines=$(curl https://newsapi.org/v2/top-headlines -s -G \
				-d sources=$newsAPISource \
				-d apiKey=$newsAPIKey \
				| jq '.articles[]')

			#read the titles and urls into an array
			readarray -t newTitles <<< $(echo $jsonHeadlines | jq -r '.title')
			readarray -t newUrls <<< $(echo $jsonHeadlines | jq -r '.url')
			#concatenate the array
			titles=("${titles[@]}" "${newTitles[@]}")
			urls=("${urls[@]}" "${newUrls[@]}")
			let i=i+1
		 done
	fi

	#display the title, and output the url to a file
	i=0
	articles=${#titles[@]}
	while [ $i -lt $articles ]; do
		echo "$leftPadding${titles[$i]}$rightPadding" > $titleFile
		polybar-msg -p $polybarPID hook newsapi 2
		echo ${urls[$i]} > $urlFile
		let i=i+1
		sleep $intervalBetweenHeadline
	done

	hM=$(( $newsAPIUpdateInterval / $intervalBetweenHeadline / $articles ))
	let hC=hC+1
done

