#!/bin/bash
#https://docs.bitfinex.com/v1/reference#rest-public-ticker
#The first argument should be the ticker symbol
#The second argument is the text in front of the information
#The third argument is the seperator
#The fourth argument is the padding
symbol=$1
pretext=$2
seperator=$3
padding=$4

tickerScript="path-to-ticker.sh"

#set 
source $(eval echo ~)/Scripts/sources

#echo "$4Updating [$symbol]...$4"

echo "$padding$pretext$($tickerScript $symbol low)$seperator$($tickerScript $symbol last_price)$seperator$($tickerScript $symbol high)$padding"
