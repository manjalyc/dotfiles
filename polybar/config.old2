#Requires Fira Mono and Awesome Fonts

########
[colors]
brblack = #002b36
brwhite = #ffffff
brgreen = #586e75
bryellow = #657b83
brblue = #839496
brcyan = #93a1a1
brred = #c14c00
brmagneta = #6c71c4
black = #073642
white = #eee8d5
yellow = #e9b200 
red = #dc322f
magneta = #d33682
blue = #20859e
cyan = #3f8b85
green = #329621 
background = ${colors.brwhite}
foreground = ${colors.black}


##########
[bar/main]
#font-0 = "Cantarell:size=8;1"
font-0 = "Fira Mono Regular:size=8;1"
font-1 = "FontAwesome:size=8;1"
#bottom = true
width = 100%
height = 20
background = ${colors.background}
foreground = ${colors.foreground}
#use on wider monitors
#separator = " | "
separator = "|"
modules-left = i3 window
modules-center = date
modules-right= memory cpu battery volume backlight wifi-network
tray-position = right
tray-maxsize = 16
tray-padding = 3
tray-transparent = tr
line-size = 2


###########
[module/i3]
type = internal/i3
format-background = ${colors.background}
wrapping-scroll = false

label-focused = %index%
label-focused-foreground = ${colors.background}
label-focused-background = ${colors.foreground}
#for mono-spaced fonts
label-focused-padding = 1
#label-focused-padding = 2
label-focused-underline = ${colors.blue}

label-unfocused = %index%
#for mono-spaced fonts
label-unfocused-padding = 1
#label-unfocused-padding = 2

label-urgent = %index%
label-urgent-foreground = ${colors.white}
label-urgent-background = ${colors.red}
label-urgent-padding = 1

label-visible = %index%
label-visible-background = ${colors.background}
label-visible-padding = 1


###############
[module/window]
type = internal/xwindow
#label-maxlen = 50
label-maxlen = 50


############
[module/cpu]
type = internal/cpu
interval = 1
format-background = ${colors.background}
format = <label>
label = "  %percentage%% "
format-foreground = ${colors.blue}
format-underline = ${colors.blue}


###############
[module/memory]
type = internal/memory
interval = 3
format-background = ${colors.background}
format = <label>
label = "  %gb_used% "
format-foreground = ${colors.cyan}
format-underline = ${colors.cyan}



################
[module/battery]
type = internal/battery
battery = BAT0
adapter = AC

format-full-background = ${colors.background}
format-full-foreground = ${colors.brgreen}
format-full-underline = ${colors.brgreen}
label-full = "  100% "

format-charging = "<animation-charging>  <label-charging> "
format-charging-background = ${colors.background}
format-charging-foreground = ${colors.green}
format-charging-underline = ${colors.green}

label-discharging = "%percentage%% (%time%) "
time-format = %H:%M
format-discharging = <ramp-capacity> <label-discharging>
format-discharging-background = ${colors.background}
format-discharging-foreground = ${colors.red}
format-discharging-underline = ${colors.red}

ramp-capacity-0 = " "
ramp-capacity-1 = " "
ramp-capacity-2 = " "
ramp-capacity-3 = " "
ramp-capacity-4 = " "

animation-charging-0 = " "
animation-charging-1 = " "
animation-charging-2 = " "
animation-charging-3 = " "
animation-charging-4 = " "
animation-charging-framerate = 750


###############
[module/volume]
type = internal/volume
master-mixer = Master

label-muted-background = ${colors.background}
label-muted = "  muted "
label-muted-foreground = ${colors.red}
label-muted-underline = ${colors.red}

format-volume-background = ${colors.background}
format-volume = "<ramp-volume> <label-volume> "
format-volume-foreground = ${colors.blue}
format-volume-underline = ${colors.blue}

ramp-volume-0 = " "
ramp-volume-1 = " "
ramp-volume-2 = " "


##################
[module/backlight]
type = internal/xbacklight
format-background = ${colors.background}
format = <label>
label = "  %percentage%% "
format-foreground =${colors.yellow}
format-underline = ${colors.yellow}

#####################
[module/wifi-network]
type = internal/network
interface = wlo1
interval = 10

label-connected = "  %signal%% "
format-connected-background = ${colors.background}
format-connected = <label-connected>
format-connected-foreground = ${colors.green}
format-connected-underline = ${colors.green}


label-disconnected = "  Down "
format-disconnected-background = ${colors.background}
format-disconnected = <label-disconnected>
format-disconnected-foreground = ${colors.red}
format-disconnected-underline = ${colors.red}


#############
[module/date]
type = internal/date
interval = 1.0
format-background = ${colors.background}
format-foreground = ${colors.brred}
format-underline = ${colors.brred}
#use on wider monitors
#date = "   %A, %B %e %Y  %X "
#moderate to small monitors
#date = "   %A, %b. %e %Y  %X "
#use for small monitors
date = "   %a, %b. %e %Y  %R "
date-alt = "  %m/%d "
