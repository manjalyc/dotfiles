#!/bin/bash

sudo pacman -S --noconfirm dunst
mkdir -p ~/.config/dunst
cp ~/Scripts/Dotfiles/dunst/dunstrc ~/.config/dunst/dunstrc
