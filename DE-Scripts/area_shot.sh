#!/bin/bash
#Takes a screenshot of an area and copies it to the clipboard
#requires gnome-screenshot xclip

mkdir -p /tmp/cm/gs
screenshot=$(mktemp -p /tmp/cm/gs --suffix=.png)

gnome-screenshot -a -f $screenshot && xclip -selection clipboard -t image/png -i $screenshot
#gnome-screenshot -a -f /tmp/8723gscreenshot.png && xclip -selection clipboard -t image/png -i /tmp/8723gscreenshot.png
