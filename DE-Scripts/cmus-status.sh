#!/bin/sh
#Icons Used:   

t=$(cmus-remote -Q  2>/dev/null| grep "tag title" | cut -c 11- | cut -f1 -d"(")
p=$(cmus-remote -Q  2>/dev/null| grep "status" | cut -c 8-)
if [[ $t = *[!\ ]* ]]
then 
	if [[ "$p" == "paused" ]]
	then
		echo " $t"
	else
		c=$(cmus-remote -Q 2>/dev/null| grep repeat_current | cut -c 20-)
		if [ "$c" == "true" ]; then echo " $t"; else echo " $t"; fi
	fi
else
	echo ""
fi

#or this one-liner v.1
#Optionally | cut -f1 -d"("
#2>/dev/null
#while true; do p=$(cmus-remote -Q 2>/dev/null| grep "tag title" | cut -c 11- | cut -f1 -d"("); if [[ $p = *[!\ ]* ]]; then p=" $p";fi; echo $p; sleep 1; done
