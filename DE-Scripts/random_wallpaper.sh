#!/bin/bash
#sets a random desktop background from a set of images in $wallpaperDir
#requires feh to set the X root background

home=$(eval echo "~")
wallpaperDir="$home/Scripts/Dotfiles/Wallpapers"

ls $wallpaperDir | grep '.jpg\|.png' | sort -R | tail -1 |while read file; do 
	echo "---"
	echo $file
	feh --bg-fill "${wallpaperDir}/${file}"; 
done
