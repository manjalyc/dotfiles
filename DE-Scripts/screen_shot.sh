#!/bin/bash
#Takes a screenshot and copies it to the clipboard
#requires gnome-screenshot xclip

mkdir -p /tmp/cm/gs
screenshot=$(mktemp -p /tmp/cm/gs --suffix=.png)
#gnome-screenshot -f /tmp/8723gscreenshot.png && xclip -selection clipboard -t image/png -i /tmp/8723gscreenshot.png
gnome-screenshot -f $screenshot && xclip -selection clipboard -t image/png -i $screenshot
